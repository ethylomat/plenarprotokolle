% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plots.R
\name{plot_word_frequency_over_time}
\alias{plot_word_frequency_over_time}
\title{Plot word frequency over time}
\usage{
plot_word_frequency_over_time(keywords, frequencies = NULL)
}
\arguments{
\item{keywords}{character vector with keywords to search for}

\item{frequencies}{dataframe with word frequencies (optional for speedup)}
}
\value{
ggplot plot element
}
\description{
Function for plotting the frequency of words over time using ggplot2.
}
\examples{
plot_word_frequency_over_time(c("Facebook", "Twitter"), frequencies = csv.read("data/word_frequencies.csv"))
plot_word_frequency_over_time(c("Facebook", "Twitter"))

}
