build_server <- function(){
  frequencies <- read.csv("data/all_frequencies.csv")
  server <- function(input, output) {
    output$distPlot <- renderPlot({
      keywords <- str_squish(str_split(input$keywords, ",")[[1]])
      plot_word_frequency_over_time(keywords, frequencies = frequencies)
    })
  }
  return(server)
}

build_ui <- function(){
  ui <- fluidPage(
    tags$style('.container-fluid, body {background-color: #d5e4eb;}'),

    fluidRow(
      column(10, offset=1, align="center",
             titlePanel("Plenarprotokolle"),
             textInput(inputId = "keywords",
                       label = "Input keyword (comma-separated):",
                       value = "facebook, twitter"),
             plotOutput(outputId = "distPlot")
      )
    )
  )
  return(ui)
}


#' BundestagsTrend Shiny App
#'
#' The BundestagsTrend Shiny App is an interactive web application for visualising
#' the word frequencies over time. The user is able to enter multiple keywords
#' (comma-seperated) to display. Running `app()` starts a development server
#' running the application.
#'
#' @import shiny
#' @import stringr
#'
#' @export
#'
app <- function(){
  shinyApp(build_ui(), build_server())
}
