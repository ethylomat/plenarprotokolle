#' Retrieve xml urls
#'
#' This function is intended for finding all available urls of
#' stenographic protocols of the 19th legislature period on the open-data page
#' of the Bundestag.
#'
#' @importFrom stringr str_c str_match_all str_extract
#' @importFrom httr GET content
#'
#' @param limit limit of expected urls (to prevent too many requests to the server)
#'
#' @return vector of urls (pointing to xml files)
#' @export
#'
retrieve_xml_urls <- function(limit = 1000){
  print("Retrieving xml urls ...")
  base_url <- "https://www.bundestag.de"

  xml_urls <- c()
  return_length <-  -1
  offset <-  0

  # walking through pages until response with zero results (hard cut at 1000th protocol)
  while (return_length != 0 && offset < limit){
    print(paste("Retrieving [", offset, " - ", offset + 10, "]"))

    # Retrieving page
    list_url = str_c(c(base_url, "/ajax/filterlist/de/services/opendata/543410-543410?limit=10&noFilterSet=true&offset=", offset), collapse = "")
    http_response <- GET(list_url)
    page_content <- content(http_response, "text")

    # Finding matching urls of xml-files
    matching_urls <- str_match_all(page_content, 'href=\"(/resource/blob/\\d+/[0-9a-z]+/\\d+-data.xml)\"')[[1]][,2]
    xml_urls = c(xml_urls, matching_urls)

    # Stop at list end
    return_length <- length(matching_urls)

    # Retrive next page
    offset <- offset + 10

    # Making sure not to send too many requests at the same time
    Sys.sleep(0.1)
  }
  str_c(base_url, xml_urls)
}

#' Download xml files
#'
#' Function for automatic download of the xml files to the local data directory
#' `/data/xml_files`. If no `xml_urls` are passed, it is downloading all
#' available documents. The files are renamed in the following syntax:
#' `<legislative_period>-<meeting>.xml`.
#'
#' @importFrom stringr str_c str_match_all str_extract
#' @importFrom httr GET content
#' @description This functions downloads all xml files to the data directory (either by passing to parameter or all by default)
#' @param vector of urls (pointing to xml files)
#' @export
#'
#' @examples
#' download_xml_files()
#' download_xml_files(xml_urls = retrieve_xml_urls(limit=10)) # download first 10 documents
#'
download_xml_files <- function(xml_urls = NULL, limit = 1000){
  # Create data directories for downloading files
  dir.create("data", showWarnings = FALSE)
  dir.create("data/xml_files", showWarnings = FALSE)

  if (is.null(xml_urls)){
    xml_urls <- retrieve_xml_urls(limit = limit)
  }

  for (url in xml_urls){
    print(paste("Downloading: ", url))
    filename <- str_extract(url, "\\d+-data.xml")
    output_file <- file.path("data/xml_files", filename)
    download.file(url = url, destfile = output_file)

    xml_file <- read_xml(output_file)
    sitzung_nr <- xml_attr(xml_file, "sitzung-nr")
    wahlperiode <- xml_attr(xml_file, "wahlperiode")
    file.rename(output_file, str_replace(output_file, coll(filename), sprintf("%s-%s.xml", wahlperiode, sitzung_nr)))
    Sys.sleep(1)
  }
}

#' Get xml files
#'
#' This function loads the xml files from the local storage (`/data/xml_files`)
#' or asks for download if the files are not present. The xml files are
#' serialized using `xml2`. If no vector is passed to the `sitzungen` attribute
#' all meetings are loaded.
#'
#' @import xml2
#' @importFrom stringr str_c str_match_all str_extract
#' @param wahlperiode integer value of wahlperiode (e.g. Wahlperiode 19)
#' @param sitzungen integer vector of meetings to load
#'
#' @return list of seralized xml documents
#'
#' @examples
#' get_xml_files(sitzungen = 1:10)
#' get_xml_files(wahlperiode = 19) # Returns documents of all meetings.
#' @export
#'
get_xml_files <- function(wahlperiode = 19, sitzungen = NULL){
  stopifnot("Wahlperiode muss Länge 1 haben" = (length(wahlperiode) == 1))

  missing_files <- FALSE
  if (is.null(sitzungen)){
    print(sprintf("Finding all available events for Wahlperiode %s", wahlperiode))
    avaliable_files <-  list.files("data/xml_files")
    wahlperiode_files <- avaliable_files[startsWith(avaliable_files, sprintf("%s-", wahlperiode))]
    if (length(wahlperiode_files) == 0){
      missing_files <-  TRUE
    }
  } else {
    # Check if all asked xml files exist -> if not, asking for download
    filenames <-  sprintf("data/xml_files/%s-%s.xml", wahlperiode, sitzungen)
    missing_files <- !all(file.exists(filenames))
  }

  if (missing_files){
    print("Some files are missing")
    user_response = askYesNo("Do you want to continue with downloading missing files from bundestag.de to your working directory?")
    if (is.na(user_response) | !user_response){
      stop("Skipping ...")
      return(NULL)
    } else if (user_response){
      download_xml_files()
    }
  }

  if (is.null(sitzungen)){
    avaliable_files <-  list.files("data/xml_files")
    filenames <- sprintf("data/xml_files/%s", avaliable_files[startsWith(avaliable_files, sprintf("%s-", wahlperiode))])
  }

  xml_files <- list()
  for (i in 1:length(filenames)){
    xml_files[[i]] = read_xml(filenames[[i]])
  }
  return(xml_files)
}

#' Get xml file
#'
#' Similar to \code{\link[=get_xml_file]{get_xml_files()}} but only returning
#' one document at the time (not as list).
#'
#'
#' @param wahlperiode integer value of wahlperiode (e.g. Wahlperiode 19)
#' @param sitzung integer of meeting to load
#'
#' @return seralized xml document
#'
#' @examples
#' get_xml_files(wahlperiode = 19, sitzung = 10)
#' get_xml_file(sitzung = 10) # equivalent
#' @export
#'
get_xml_file <- function(wahlperiode = 19, sitzung = 1){
  get_xml_files(wahlperiode = wahlperiode, sitzungen = sitzung)[[1]]
}
