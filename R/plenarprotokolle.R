#' \pkg{plenarprotokolle}
#' @keywords Sitzung, Redner, Rede
#' @details
#' Für die Analyse dieser Protokolle haben wir folgende Funktionen bereitstellt:\cr
#'
#' ### Daten
#' \describe{
#'   \item{\code{\link[=retrieve_xml_urls]{retrieve_xml_urls()}}}{Funktion zum abrufen der verfügbaren stenographischen Protokolle.}
#'   \item{\code{\link[=download_xml_files]{download_xml_files()}}}{Funktion zum automatischen herunterladen der xml Dateien vom Server des Bundestages.}
#'   \item{\code{\link[=get_xml_files]{get_xml_files()}}\cr\code{\link[=get_xml_file]{get_xml_file()}}}{Funktionen zum abrufen von xml-Protokollen. Falls lokal vorhanden wird die Datei lokal geladen.}
#' }
#'
#' ### Parsing
#' \describe{
#'   \item{\code{\link[=getReden]{getReden()}}}{Funktion zum Parsen von Reden aus den xml Dateien.}
#'   \item{\code{\link[=getRednerInfo]{getRednerInfo()}}}{Funktion zum Parsen der Redner-Informationen aus den xml Dateien.}
#'   \item{\code{\link[=get_sitzung]{get_sitzung()}}}{Funktionen zum abrufen von Metadaten zu den Sitzungen.}
#'   \item{\code{\link[=redner_id]{redner_id()}}}{Funktionen zum Parsen der redner IDs aus den Dokumenten.}
#' }
#'
#' ### Auswertungen
#' \describe{
#'   \item{\code{\link[=word_freq]{word_freq()}}}{Funktion zur Auswertung der Wort-Häufigkeiten in Sitzungen.}
#'   \item{\code{\link[=top_words]{top_words()}}}{Funktion zur Auswertung der häufigsten Wörter in verschiedenen Sitzungen}
#' }
#'
#' ### Visualisierungen
#' \describe{
#'   \item{\code{\link[=plot_word_frequency_over_time]{plot_word_frequency_over_time()}}}{Funktion zum Plotten von Wort-Häufigkeiten über die Zeit mit Hilfe von ggplot.}
#'   \item{\code{\link[=app]{app()}}}{BundestagsTrend Shiny App zur Visualisierung der Wort-Häufigkeiten über die Zeit.}
#' }
#'
#' @references
#' \href{https://www.bundestag.de/services/opendata}{Hier} sind die Plenarprotokolle der Reden im Deutschen Bundestag. \href{https://www.bundestag.de/resource/blob/577234/f9159cee3e045cbc37dcd6de6322fcdd/dbtplenarprotokoll_kommentiert-data.pdf}{Hier} ist die genaue Beschreibung der Elemente der XML-Dateien.
#'
#' @keywords Datenauswertung, Plenarprotokolle, Textanalyse
"_PACKAGE"
# The following block is used by usethis to automatically manage
# roxygen namespace tags. Modify with care!
## usethis namespace: start
## usethis namespace: end
NULL
