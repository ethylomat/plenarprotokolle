library(tidyverse)
library(xml2)
library(tidytext)
library(stopwords)
library(digest)

extract_reden <- function(sitzungen){
  reden <- lapply(sitzungen, function (xml) {xml_find_all(xml, ".//rede")})
  reden <- unlist(reden, recursive = F)
  reden
}

get_kommentare <- function(reden){
  reden_ids <- lapply(reden, function(xml) {xml_attr(xml, 'id')})
  kommentare <- lapply(reden, function(xml) {xml_find_all(xml, ".//kommentar")})
  
  tibble(reden_ids=unlist(reden_ids), kommentare=kommentare) %>%
    mutate(type = kommentare %>% map(~ xml_name(.)),
           content = kommentare %>% map(~ xml_text(.))) %>%
    #i = kommentare %>% map(~ seq_along(.))) 
    select(reden_ids, type, content) %>%
    unnest(cols = c(type, content))
}

get_reden <- function(reden){
  reden_ids <- lapply(reden, function(xml) {xml_attr(xml, 'id')})
  paragraphs <- lapply(reden, function(xml) {xml_find_all(xml, ".//p")})
  
  tibble(reden_ids=unlist(reden_ids), paragraphs=paragraphs) %>%
    mutate(type = paragraphs %>% map(~ xml_name(.)),
           content = paragraphs %>% map(~ xml_text(.))) %>%
    #i = kommentare %>% map(~ seq_along(.))) 
    select(reden_ids, type, content) %>%
    unnest(cols = c(type, content))
}

files <- file.path("data", list.files("data"))
#files <- head(files, 2)

sitzungen <- lapply(files, read_xml)
reden <- extract_reden(sitzungen)
reden

comments <- get_kommentare(reden)
reden <- get_reden(reden)

reden %>% 
  unnest_tokens(word, content) -> words

reden

custom_stopwords <- c(
  "geehrte",
  "dass",
  "müssen",
  "herr",
  "mehr",
  "kollegen",
  "schon",
  "herren",
  "damen",
  "menschen",
  "dank",
  "vielen",
  "gibt",
  "geht",
  "kolleginnen",
  "heute",
  "liebe",
  "ja",
  "ganz",
  "sagen",
  "immer",
  "frau",
  "dafür",
  "fraktion",
  "kollege",
  "unsere",
  "frage"
)
entity_stopwords <- c(
  "cdu",
  "afd",
  "csu",
  "spd"
)
sw <- tibble(word=c(stopwords("de"), custom_stopwords, entity_stopwords))
words %>%
  anti_join(sw) %>% 
  count(word, sort = TRUE)

words %>%
  anti_join(sw) %>%
  count(word, sort = TRUE)

words
